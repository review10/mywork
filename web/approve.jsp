<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>
<%
try{
	String req_name=request.getParameter("req_name");
	int req_id=Integer.parseInt(request.getParameter("req_id"));
	String del_entry="delete from requests where user_id=? and g_name=?";
	
	String add_mem="insert into "+req_name+"(user_id) select * from(select ? as user_id) as t where not exists(select user_id from "+req_name+" where user_id=?) limit 1";
	
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	
	PreparedStatement stat=myConnection.prepareStatement(del_entry);
	stat.setInt(1,req_id);
	stat.setString(2,req_name);
	stat.executeUpdate();
	
	PreparedStatement stat2=myConnection.prepareStatement(add_mem);
	stat2.setInt(1,req_id);
	stat2.setInt(2,req_id);
	stat2.executeUpdate();
	response.sendRedirect("myorgs.jsp");
}

 catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}%>

