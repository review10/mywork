<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>login</title>
<link rel="stylesheet" href="style.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<style>
.bg_one {
    display: block;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: url(images/bg.svg) transparent;
    background-size: auto 100%;
    z-index: -1;
}
.signin_container {
    display: block;
    width: 890px;
    min-height: 520px;
    background-color: #fff;
    box-shadow: 0px 2px 30px #ccc6;
    margin: auto;
    position: relative;
    z-index: 1;
    margin-top: 7%;
    overflow: hidden;
}
.zoho_logo {
    display: block;
    height: 30px;
    width: auto;
    margin-bottom: 20px;
    background: url(images/zoho-logo-zh.png) no-repeat transparent;
    background-size: auto 100%;
}
.signinbox{
	width: 500px;
    min-height: 520px;
    height: auto;
    background: #fff;
    box-sizing: border-box;
    padding: 50px 50px;
    border-radius: 2px;
    transition: all .1s ease-in-out;
    float: left;
    overflow-y: auto;
    display: table-cell;
    border-right: 2px solid #f1f1f1;
}
.signin_head {
    display: block;
    font-size: 24px;
    font-weight: 500;
    margin-bottom: 30px;
    line-height: 30px;
    transition: all .1s ease-in-out;
}
.service_name, .backup_desc, .pass_name {
    display: block;
    font-size: 16px;
    color: #000;
    font-weight: 400;
}
.textbox {
    display: block;
    width: 100%;
    height: 44px;
    box-sizing: border-box;
    border-radius: 2px;
    text-indent: 12px;
    font-size: 16px;
    outline: none;
    border: none;
    padding-right: 12px;
    transition: all .2s ease-in-out;
    background: #f8f8f8;
    border: 1px solid #E4E4E4;
}

input[type=submit] {

    background-color: #159AFF;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
    text-transform: uppercase;
     cursor: pointer;
    display: block;
    width: 100%;
    height: 44px;
    border-radius: 4px;
}
#signuplink {
    width: 100%;
    display: inline-block;
    margin: 30px auto;
    text-align: center;
    line-height: 20px;
    font-weight: 400;
    font-size: 14px;
    color: #555;
}
#underline{
            text-decoration: none;
}
.signin_fed_text {
    padding-bottom:10px;
    padding-top:20px;
    display: block;
    font-size: 15px;
    font-weight: 500;
    color: #444;

}
.google_icon1 {
    background-color: #fff;
    width: auto;
    box-shadow: 0px 0px 2px #00000012, 0px 2px 2px #00000024;
    border-radius: 5px;
    margin-right: 10px;
    margin-bottom: 10px;
    height: 40px;
    cursor: pointer;
    overflow: hidden;
    float: left;
}
}
.google_icon {
padding-right:30px;
    float:left;
    background-size: 205px auto;
    background-position: -10px -204px;
    width: 75px;
    margin: 11px 8px;
    border-radius: 5px;
    margin-right: 10px;
    margin-bottom: 10px;
    overflow: hidden;
}
</style>
</head>

<body>
<%
String login_msg="";
login_msg=(String)session.getAttribute("login_msg");
%>
<header>

<h1></h1>

<div class="bg_one"></div>
<div class="signin_container">
	<div class="signinbox">
		<div class="zoho_logo"></div>
		<div class="signin_head">
			<span id="headtitle">Sign in </span>
			<span id="trytitle"></span>
				<div class="service_name">to access Accounts</div>
			
	        </div>
	        
<form action="login2.jsp" method="post">
<%if(login_msg!=null){%>
<p style="color:#f0483e;height: 25px;
    line-height: 25px;
    padding: 0 10px;"> <%=login_msg%></p>
<%}%>
<input type="number" name="user_id" placeholder="user id" class="textbox" /><br>
<input type="password" name="password" placeholder="password" class="textbox"/><br>
<input type="submit" value="sign in"/>

</form>
<div class="signin_fed_text">Sign in using</div>


<a href="gmail_signin.jsp" style="padding-right:20px;">
    <img src="images/gmail.png" width="35" height="25" />
</a>
<a href="phone_signin.jsp" >
    <img src="images/phone.png" width="35" height="25" />
</a>

<div id="signuplink">Don't have a Zoho account ?<a style="color:#03a9f5;font-weight: 600;font-size:15px;" id ="underline" href="x3.jsp">Sign Up Now</a>

</div>
</div>
</header>		
</body>


</html>
