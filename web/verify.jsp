<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>
<%
try{
	String verify=request.getParameter("verify");
	int v_code=Integer.parseInt(request.getParameter("v_code"));
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	Statement stat=myConnection.createStatement();
	if(verify.equals("mail")){
		String v_mail=(String)session.getAttribute("v_mail");
		String login_q="select user_id from user_mail where email='"+v_mail+"' and verify_id=?";
		PreparedStatement stat2=myConnection.prepareStatement(login_q);
		stat2.setInt(1,v_code);
		ResultSet rs=stat2.executeQuery();
		int check=0;
		while(rs.next()){
			check=1;
			int user_id=rs.getInt(1);
			session.setAttribute("user_id",user_id);
			ResultSet rs2=stat.executeQuery("Select firstname from registration where user_id='"+user_id+"'");
			while(rs2.next()){
				String firstname=rs2.getString(1);
				session.setAttribute("firstname",firstname);
			}
			response.sendRedirect("welcome.jsp");
		}
		if(check==0){
			response.sendRedirect("verify_html.jsp?verify=mail&error=wrong_code");
		}
	}	
	else{
		String v_phno=(String)session.getAttribute("v_phno");
		String login_q="select user_id from user_phno where ph_no='"+v_phno+"' and verify_id=?";
		PreparedStatement stat2=myConnection.prepareStatement(login_q);
		stat2.setInt(1,v_code);
		ResultSet rs=stat2.executeQuery();
		int check=0;
		while(rs.next()){
			check=1;
			int user_id=rs.getInt(1);
			session.setAttribute("user_id",user_id);
			ResultSet rs2=stat.executeQuery("Select firstname from registration where user_id='"+user_id+"'");
			while(rs2.next()){
				String firstname=rs2.getString(1);
				session.setAttribute("firstname",firstname);
			}
			response.sendRedirect("welcome.jsp");
		}
		if(check==0){
			response.sendRedirect("verify_html.jsp?verify=phno&error=wrong_code");
		}
	}
	
}
catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}
%>
