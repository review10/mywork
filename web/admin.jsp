<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,javax.sql.*"%>

<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.ztopbar {
    display: block;
    height: 54px;
    width: 100%;
    background-color: #fff;
    position: fixed;
    z-index: 5;
    box-shadow: 0px 0px 2px #ccc;
    top: 0px;
}
.zoho_logo {
    display: inline-block;
    height: 24px;
    width: 168px;
    margin: 15px 0px;
    margin-left: 50px;
    background: url(images/zoho-accounts-logo.png) no-repeat transparent;
    background-size: auto 100%;
    cursor: pointer;
}
.vertical-menu {
margin: 15px 0px;
    margin-left: 50px;
  width: 200px;
}

.vertical-menu a {
  background-color: #011722;
  color: white;
  display: block;
  padding: 12px;
  text-decoration: none;
}

.vertical-menu a:hover {
  background-color:  #ffffff08;
}

.vertical-menu a.active {
  background-color: #ffffff08;
  color: white;
}
.navbar {
    height: 100%;
    width: 300px;
    display: block;
    position: fixed;
    left: 0px;
    top: 54px;
    z-index: 5;
    background-color: #011722;
    transition: all .2s ease-in-out;
}
.submenu_div {
text-decoration: none;
padding-left: 30px;
    padding-bottom: 15px;
    margin: 15px 0px;
    margin-left: 15px;
    width: 100px;
    color: white;
}
.center{
    text-align: center;
    position:absolute;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; 
    margin-left: -15em; 
}
</style>
</head>
<body>

<%try{
	int user_id=(int)session.getAttribute("user_id");
	String u_type=(String)session.getAttribute("u_type");
	
%>
<div class="ztopbar">
<span class="zoho_logo" style="margin-left:50px;"></span>
</div>

<div class="navbar" style="height=100%;overflow:unset;">
<div class="vertical-menu">
  <a href="welcome.jsp" >Home</a>
  <a href="profile.jsp">Profile</a>
  <a id= "underline"  href="myorgs.jsp" >My Orgs</a>
  <%if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin")))%>
  <a href="admin.jsp" class="active">Admin</a>
 
<div class="submenu_div">
<a  href="admin.jsp">org info</a>
</div>
</div>
</div>
<div class="center">
    <form action="searchorg.jsp">
      <p>Group Name
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search" style="border: 1px solid #9aa0a6;font-size:15px"></i></button>
      </p>
    </form>
</div>


<%}
 catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}%>
</body>
</html>



	
