<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>

<html>
<head>
<style>
.main {
    display: block;
    width: 560px;
    height: auto;
    background-color: #fff;
    box-shadow: 0px 2px 30px #ccc6;
    margin: auto;
    position: relative;
    z-index: 1;
    margin-top: 3%;
    overflow: hidden;
}
.inner-container {
    padding: 50px 50px;
    text-align: left;
    overflow: auto;
}
.zoho_logo {
    display: block;
    height: 30px;
    width: auto;
    margin-bottom: 20px;
    background: url(images/zoho-logo-zh.png) no-repeat transparent;
    background-size: auto 100%;
}
.signuptitle{
    display: block;
    font-size: 24px;
    font-weight: 500;
    margin-bottom: 30px;
    line-height: 30px;
    transition: all .1s ease-in-out;
}
input[type="text"], input[type="email"], input[type="password"], input[type="number"], textarea, select {
    display: block;
    width: 100%;
    height: 44px;
    box-sizing: border-box;
    border-radius: 2px;
    text-indent: 12px;
    font-size: 16px;
    outline: none;
    border: none;
    padding-right: 12px;
    transition: all .2s ease-in-out;
    background: #f8f8f8;
    border: 1px solid #E4E4E4;
    margin-top: 25px;
}
#firstname, #lastname {
    display: inline-block;
    margin-top: 0px;
    padding: 1px 2px;
}
#underline{
            text-decoration: none;
        }
.bg_one {
    display: block;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: url(images/bg.svg) transparent;
    background-size: auto 100%;
    z-index: -1;
}
input[type=submit] {
    border-radius: 2px;
    margin-top: 25px;
    padding-right: 12px;
    cursor: pointer;
    display: block;
    width: 100%;
    height: 44px;
    border-radius: 4px;
    letter-spacing: .5px;
    font-size: 14px;
    font-weight: 600;
    outline: none;
    border: none;
    text-transform: uppercase;
    margin-bottom: 30px;
    transition: all .2s ease-in-out;
    box-shadow: 0px 2px 2px #fff;
    background-color: #159AFF;
    color: #fff;
    
}
.alert {
  opacity: 1;
  transition: opacity 0.6s; /* 600ms to fade out */
}
.zlogin {
	padding: 50px 50px;
    text-align:center;
    overflow: auto;
}
</style>
</head>
<body>
<header>
<%
String msg=request.getParameter("msg");
if("notMatch".equals(msg)){
%>
<h3 class="alert">New password and confirm password doesnot match!</h3>
<%}%>
<%
if("wrong".equals(msg)){
%>
<h3 class="alert">Your old password is wrong!</h3>
<%}%>
<%
if("done".equals(msg)){
%>
<h3 class="alert">Password change successfully!</h3>
<%}%>
<%
if("invalid".equals(msg)){
%>
<h3 class="alert">Something went wrong!Try again!</h3>
<%}%>

<div class="bg_one"></div>
<div align="center" class="main">
	<div class="inner-container">

		<div class="zoho_logo"></div>
		
		<div class="signuptitle">Change Password</div>
	
<form action="passwordaction.jsp" method="post">
<input type="password" name="oldpassword" placeholder="Old Password"/>
<input type="password" name="newpassword" placeholder="New Password"/>
<input type="password" name="confirmpassword" placeholder="Re-Enter Password"/>
<input type="submit" value="Save"/>
	<a id= "underline" class="zlogin" href="welcome.jsp">HOME</a>
</header>
</form>

</div>
</div>
</body>
</html>
