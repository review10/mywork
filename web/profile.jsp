<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,javax.sql.*"%>


<html>
<head>
<style>
.ztopbar {
    display: block;
    height: 54px;
    width: 100%;
    background-color: #fff;
    position: fixed;
    z-index: 5;
    box-shadow: 0px 0px 2px #ccc;
    top: 0px;
}
.zoho_logo {
    display: inline-block;
    height: 24px;
    width: 168px;
    margin: 15px 0px;
    margin-left: 50px;
    background: url(images/zoho-accounts-logo.png) no-repeat transparent;
    background-size: auto 100%;
    cursor: pointer;
}
.vertical-menu {
margin: 15px 0px;
    margin-left: 50px;
  width: 200px;
}

.vertical-menu a {
  background-color: #011722;
  color: white;
  display: block;
  padding: 12px;
  text-decoration: none;
}

.vertical-menu a:hover {
  background-color:  #ffffff08;
}

.vertical-menu a.active {
  background-color: #ffffff08;
  color: white;
}
.navbar {
    height: 100%;
    width: 300px;
    display: block;
    position: fixed;
    left: 0px;
    top: 54px;
    z-index: 5;
    background-color: #011722;
    transition: all .2s ease-in-out;
}
.box {
    padding-bottom: 10px;
    width: 100%;
    height: auto;
    /* min-height: 300px; */
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0px 2px 2px #00000012;
    box-sizing: border-box;
    margin-bottom: 30px;
    padding: 40px;
}
.profileinfo_form {
    display: flex;
    flex-wrap: wrap;
    max-width: 1000px;
    position: relative;
    overflow: hidden;
    outline: none;
}
.textbox {
    margin-top: 40px;
    margin-bottom: 0px;
    height: 42px;
    pointer-events: none;
    margin-right: 50px;  
    display: inline-block;
    overflow: auto;
    cursor: default;
    width:500px;
}
.textbox_label {
    color: #333;
    display: block;
    font-size: 12px;
    line-height: 16px;
    margin-bottom: 5px;
    font-weight: 500;
}
.textbox_data {
    color: #000;
    border: none;
    font-size: 16px;
    text-indent: 0px;
    height: auto;
}
.content_div {
	background-color: #E9EAED;
    display: block;
    padding-left: 350px;
    padding-right: 50px;
    margin: auto;
    margin-top: 80px;
    height: auto;
    transition: all .2s ease-in-out;
    max-width: 1580px;
    overflow-x: hidden;
}
.page_head {
    margin-top: 20px;
    display: block;
    font-size: 24;
    margin-bottom: 24px;
    line-height: 28px;
}
.zlogin {
        background-color: #10bc83;
    color: #fff;
    display: inline-block;
    box-sizing: border-box;
    margin-top: 10px;
    font-size: 14px;
    font-weight: 500;
    line-height: 40px;
    position: relative;
    text-align: center;
    border-radius: 4px;
    padding: 0px 30px;
    overflow: hidden;
    float:right;
}
#underline{
            text-decoration: none;
        }
</style>
</head>
<body>
<%try{
	String fname="";
	String lname="";
	String mail="";
	String country="";
	String phno="";
	
	int user_id=(int)session.getAttribute("user_id");
	String u_type=(String)session.getAttribute("u_type");
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	
	String fetch_f_q="SELECT firstname FROM  registration WHERE user_id=?";
	String fetch_l_q="SELECT lastname FROM  registration WHERE user_id=?";
	String fetch_m_q="SELECT email_id FROM  registration WHERE user_id=?";
	String fetch_c_q="SELECT country FROM  registration WHERE user_id=?";
	String fetch_p_q="SELECT ph_no FROM  registration WHERE user_id=?";
	
	PreparedStatement stat=myConnection.prepareStatement(fetch_f_q);
	stat.setInt(1,(user_id));
	ResultSet myResultSet=stat.executeQuery();
	while(myResultSet.next())
	fname=myResultSet.getString(1);
	
	PreparedStatement stat1=myConnection.prepareStatement(fetch_l_q);
	stat1.setInt(1,(user_id));
	myResultSet=stat1.executeQuery();
	while(myResultSet.next())
	lname=myResultSet.getString(1);
	
	PreparedStatement stat2=myConnection.prepareStatement(fetch_m_q);
	stat2.setInt(1,(user_id));
	myResultSet=stat2.executeQuery();
	while(myResultSet.next())
	mail=myResultSet.getString(1);
	
	PreparedStatement stat3=myConnection.prepareStatement(fetch_c_q);
	stat3.setInt(1,(user_id));
	myResultSet=stat3.executeQuery();
	while(myResultSet.next())
	country=myResultSet.getString(1);
	
	PreparedStatement stat4=myConnection.prepareStatement(fetch_p_q);
	stat4.setInt(1,(user_id));
	myResultSet=stat4.executeQuery();
	while(myResultSet.next())
	phno=myResultSet.getString(1);
	
%>


<div class="ztopbar">
<span class="zoho_logo" style="margin-left:50px;"></span>
</div>

<div class="navbar" style="height=100%;overflow:unset;">
<div class="vertical-menu">
  <a href="welcome.jsp" >Home</a>
  <a href="profile.jsp" class="active">Profile</a>
  <a id= "underline"  href="myorgs.jsp" >My Orgs</a>
  <%if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin"))){%>
  <a href="admin.jsp">Admin</a>
  <%}%>
</div>
</div>
<div class="content_div">
<div class="page_head">Profile
<a id= "underline" class="zlogin" href="changepword.jsp?user_id=${user_id}">Change Password</a></div>
<div class="box">

	<div class="profileinfo_form" tabindex="0">
		<div class="textbox">
			<label class="textbox_label">User ID</label>
			<label class="textbox_data"><%=user_id%></label>
	        </div>
	        <div class="textbox">
			<label class="textbox_label">User Type</label>
			<label class="textbox_data"><%=u_type%></label>
	        </div>
		<div class="textbox">
			<label class="textbox_label">First Name</label>
			<label class="textbox_data"><%=fname%></label>
	        </div>
	        <div class="textbox">
			<label class="textbox_label">Last Name</label>
			<label class="textbox_data"><%=lname%></label>
	        </div>
	        <div class="textbox">
			<label class="textbox_label">Email ID</label>
			<label class="textbox_data"><%=mail%></label>
	        </div>
	        <div class="textbox">
			<label class="textbox_label">Phone Number</label>
			<label class="textbox_data"><%=phno%></label>
	        </div>
	        <div class="textbox">
			<label class="textbox_label">Country</label>
			<label class="textbox_data"><%=country%></label>
	        </div>
	        
	</div>
</div>
</div>


<%}
 catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}%>
</body>
</html>



