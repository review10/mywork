<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>

<html>
<head>
<style>
p{
	margin:25px 0;
    text-align: center;
    position:center;
    
}
.icon{
    box-sizing: border-box;
    width: 100%;
    padding: 8px;
    justify-content: space-between;
    float:right;
    text-align:right;
}
#underline{
            text-decoration: none;
        }
.content-table{
	border-collapse:collapse;
	margin:25px 0;
	font-size:0.9em;
	min-width:400px;
	border-radius:5px 5px 0 0;
	overflow:hidden;
	box-shadow: 0 0 20px rgba(0,0,0,0.15);
	margin-left: auto;
        margin-right: auto;
}
.content-table thead tr{
	background-color:#009879;
	color:#ffffff;
	text-align:center;
	font-weight:bold;
}
.content-table thead th, .content-table td{
	padding: 12px 15px;
	text-align:center;
	
}
.content-table tbody tr{
	border-bottom:1px solid #dddddd;
}
.content-table tbody tr:last-of-type{
	border-bottom:2px solid #009879;
}
#underline{
            text-decoration: none;
        }
.zlogin {
    background: #f0483e;
    color: #fff;
    display: inline-block;
    font-size: 12px;
    font-weight: 600;
    height: 30px;
    line-height: 30px;
    padding: 0 20px;
    text-align: center;
    text-transform: uppercase;
    box-sizing: border-box;
    margin-left: 40%;
    overflow:hidden;
}
.z2login {
    background: #f0483e;
    color: #fff;
    display: inline-block;
    font-size: 12px;
    font-weight: 600;
    height: 30px;
    line-height: 30px;
    padding: 0 20px;
    text-align: center;
    text-transform: uppercase;
    box-sizing: border-box;
    margin-left: 13%;
    overflow:hidden;
}
.alert {
  opacity: 1;
  transition: opacity 0.6s; /* 600ms to fade out */
}
.zlogin {
	text-decoration: none;
        background-color: #10bc83;
    color: #fff;
    display: inline-block;
    box-sizing: border-box;
    margin-top: 10px;
    font-size: 14px;
    font-weight: 500;
    line-height: 40px;
    position: relative;
    text-align: center;
    border-radius: 4px;
    padding: 0px 30px;
    overflow: hidden;
    float:left;
}
</style>
</head>
<body>
<header>
<%
String g_name=request.getParameter("g_name");
session.setAttribute("g_name",g_name);
String u_type=(String)session.getAttribute("u_type");
int user_id=(int)session.getAttribute("user_id");  
PreparedStatement stat=null;
PreparedStatement stat2=null;
PreparedStatement stat3=null;
DataSource ds;
String fetch="Select * from "+g_name;
String fetch_type="Select g_type from temp_data where g_names=?";
String g_type="";
int g_member;
String g_mem_fetch="Select u_type from registration where user_id=?";
String g_mem_type="";
try{	
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	stat=myConnection.prepareStatement(fetch);
	ResultSet rs=stat.executeQuery();
	stat2=myConnection.prepareStatement(fetch_type);
	stat2.setString(1,g_name);
	ResultSet rs2=stat2.executeQuery();
%>

<%
String joinmsg=request.getParameter("joinmsg");
if("exists".equals(joinmsg)){
%>
<h3 class="alert">Already in the Org!</h3>
<%}

if("done".equals(joinmsg)){
%>
<h3 class="alert">Successfully Joined!</h3>
<%}
if("requestadmin".equals(joinmsg)){
%>
<h3 class="alert">Request sent to admin!</h3>
<%}%>

	
<% while(rs2.next()){
	g_type=rs2.getString(1);
	 %>
<p><%= g_type %></p>
<%}%>

<% if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin"))){
if(g_type.equals("public")){ %>
<a class="zlogin" href="org_changes.jsp?g_name=<%=g_name%>&g_type=<%=g_type%>">Make private</a>
<%}else{ %>
<a class="zlogin" href="org_changes.jsp?g_name=<%=g_name%>&g_type=<%=g_type%>">Make public</a>
<%}}%>

<form action="remove_users.jsp">
<TABLE class="content-table">
      <thead>
      	  <tr>
      	    <th>&nbsp; </th>
            <th><%=g_name%></th>
          </tr>
      </thead>
      
      <tbody>
      <%
	while(rs.next()){
		g_member=rs.getInt(1);
		stat3=myConnection.prepareStatement(g_mem_fetch);
		stat3.setInt(1,g_member);
		ResultSet rs3=stat3.executeQuery();
		while(rs3.next()){
		g_mem_type=rs3.getString(1);}
		if("Admin".equals(u_type)){
			if(("Admin".equals(g_mem_type)) || ("SuperAdmin".equals(g_mem_type))){  %>
	   			<tr>
	   				<td> </td>
	   				<td><%= g_member%></td>
	   			</tr>
			<%}%>
			<%if( ("User".equals(g_mem_type))){%>
				<tr>
					<td><input type="checkbox" name="selected_users" value=<%=g_member%>></td>
         				<td><a id="underline" href="showdetails.jsp?g_member=<%= g_member%>" ><%= g_member%></a></td>
       			</tr>
       	<%}}%>
       
       	<%if("SuperAdmin".equals(u_type)){%>
       		<tr>
       			<td><input type="checkbox" name="selected_users" value=<%=g_member%>></td>
         			<td><a id="underline" href="showdetails.jsp?g_member=<%= g_member%>" ><%= g_member%></a></td>
      			</tr>
       
      		<%}if("User".equals(u_type)) {%>
     	 		<tr>
     	 			<td> </td>
         			<td><%= g_member%></td>
        		</tr>
       	<% }
       }%>
     </tbody>
</TABLE>
<%if( ("Admin".equals(u_type) )|| ("SuperAdmin".equals(u_type))){%>
<input type="submit" value="Remove members">
<%}%>
</form>

<a id= "underline" class="zlogin" href="joingrpaction.jsp">JOIN</a>
<%if(u_type.equals("Admin") || u_type.equals("SuperAdmin")){%>
<a id= "underline" class="z2login" href="addusers.jsp">ADD</a>
<%
}}
       catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}
 %>
 </header>
</body>
</html>
