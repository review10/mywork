<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>

<html>
<head>
<style>
.content-table{
	border-collapse:collapse;
	margin:25px 0;
	font-size:0.9em;
	min-width:400px;
	border-radius:5px 5px 0 0;
	overflow:hidden;
	box-shadow: 0 0 20px rgba(0,0,0,0.15);
	margin-left: auto;
        margin-right: auto;
}
.content-table thead tr{
	background-color:#009879;
	color:#ffffff;
	text-align:center;
	font-weight:bold;
}
.content-table thead th, .content-table td{
	padding: 12px 15px;
	text-align:center;
	
}
.content-table tbody tr{
	border-bottom:1px solid #dddddd;
}
.content-table tbody tr:last-of-type{
	border-bottom:2px solid #009879;
}

</style>
</head>
<body>
<%
int g_member=Integer.parseInt(request.getParameter("g_member"));
String fetch_detail1="Select firstname from registration where user_id=?";
String fetch_detail2="Select lastname from registration where user_id=?";
String fetch_detail3="Select ph_no from registration where user_id=?";
String fetch_detail4="Select country from registration where user_id=?";
String fetch_detail5="Select password from registration where user_id=?";
String fetch_detail6="Select email_id from registration where user_id=?";
String fetch_detail7="Select u_type from registration where user_id=?";
PreparedStatement stat1,stat2,stat3,stat4,stat5,stat6,stat7;

try{	
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	
	stat1=myConnection.prepareStatement(fetch_detail1);
	stat1.setInt(1,g_member);
	ResultSet rs1=stat1.executeQuery();
	
	stat2=myConnection.prepareStatement(fetch_detail2);
	stat2.setInt(1,g_member);
	ResultSet rs2=stat2.executeQuery();
	
	stat3=myConnection.prepareStatement(fetch_detail3);
	stat3.setInt(1,g_member);
	ResultSet rs3=stat3.executeQuery();
	
	stat4=myConnection.prepareStatement(fetch_detail4);
	stat4.setInt(1,g_member);
	ResultSet rs4=stat4.executeQuery();
	
	stat5=myConnection.prepareStatement(fetch_detail5);
	stat5.setInt(1,g_member);
	ResultSet rs5=stat5.executeQuery();
	
	stat6=myConnection.prepareStatement(fetch_detail6);
	stat6.setInt(1,g_member);
	ResultSet rs6=stat6.executeQuery();
	
	stat7=myConnection.prepareStatement(fetch_detail7);
	stat7.setInt(1,g_member);
	ResultSet rs7=stat7.executeQuery();
	
%>
<TABLE class="content-table">
      <thead>
      	  <tr>
      	    <th>User Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Country</th>
            <th>Password</th>
            <th>Email Id</th>
            <th>User Type</th>
          </tr>
      </thead>
      
      <tbody>
	<tr>
         <td><%= g_member%></td>
     
       <%while(rs1.next()) { %>
      
         <td><%=rs1.getString(1)%></td>
     <%}%>
       <%while(rs2.next()) {%>
     
         <td><%=rs2.getString(1)%></td>
    <%}%>
       <%while(rs3.next()) {%>
 
         <td><%=rs3.getString(1)%></td>
     <%}%>
       <%while(rs4.next()){ %>
    
         <td><%=rs4.getString(1)%></td>
     <%}%>
       <%while(rs5.next()) {%>
   
         <td><%=rs5.getString(1)%></td>
      <%}%>
       <%while(rs6.next()) {%>
  
         <td><%=rs6.getString(1)%></td>
   <%}%>
       <%while(rs7.next()) {%>
  
         <td><%=rs7.getString(1)%></td>
         <%}%>
       </tr>

     </tbody>
</TABLE>
</body>
<%
}
       catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}
 %>

