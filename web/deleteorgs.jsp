<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,javax.annotation.Resource,java.util.*"%>
<%
try{
	InitialContext ic=new InitialContext();
        Context ctx=(Context)ic.lookup("java:comp/env");
        DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
	Connection myConnection=ds.getConnection();
	Statement stat=myConnection.createStatement();
	String selected_orgs[]= request.getParameterValues("selected_orgs");
	for(int i=0; i<selected_orgs.length; i++){
		stat.executeUpdate("drop table "+selected_orgs[i]);
		stat.executeUpdate("delete from temp_data where g_names="+selected_orgs[i]);
	}
	response.sendRedirect("myorgs.jsp");
}
 catch(Exception e){
	System.out.println("error occured");
	System.out.println(e.getMessage());
}
%>
