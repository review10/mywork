<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,java.util.*"%>

<html>
<head>
<style>
.main {
    display: block;
    width: 560px;
    height: auto;
    background-color: #fff;
    box-shadow: 0px 2px 30px #ccc6;
    margin: auto;
    position: relative;
    z-index: 1;
    margin-top: 3%;
    overflow: hidden;
}
.inner-container {
    padding: 50px 50px;
    text-align: left;
    overflow: auto;
}
.zoho_logo {
    display: block;
    height: 30px;
    width: auto;
    margin-bottom: 20px;
    background: url(images/zoho-logo-zh.png) no-repeat transparent;
    background-size: auto 100%;
}
.bg_one {
    display: block;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: url(images/bg.svg) transparent;
    background-size: auto 100%;
    z-index: -1;
}
input[type="text"] {
    display: block;
    width: 100%;
    height: 44px;
    box-sizing: border-box;
    border-radius: 2px;
    text-indent: 12px;
    font-size: 16px;
    outline: none;
    border: none;
    padding-right: 12px;
    transition: all .2s ease-in-out;
    background: #f8f8f8;
    border: 1px solid #E4E4E4;
    margin-top: 25px;
}
input[type=submit] {
    border-radius: 2px;
    margin-top: 25px;
    padding-right: 12px;
    cursor: pointer;
    display: block;
    width: 100%;
    height: 44px;
    border-radius: 4px;
    letter-spacing: .5px;
    font-size: 14px;
    font-weight: 600;
    outline: none;
    border: none;
    text-transform: uppercase;
    margin-bottom: 30px;
    transition: all .2s ease-in-out;
    box-shadow: 0px 2px 2px #fff;
    background-color: #159AFF;
    color: #fff;
    
}
.alert {
  opacity: 1;
  transition: opacity 0.6s; /* 600ms to fade out */
}
#underline{
            text-decoration: none;
}
</style>
</head>
<body>
<header>
<%
String error=request.getParameter("error");
if("wrong_phno".equals(error)){ 
 %>
<h3 class="alert">Wrong Phone Number!</h3>

<%}

if("wrong_email".equals(error)){ 
%>
<h3 class="alert">Wrong Email ID!</h3>

<%} 
String verify=request.getParameter("verify");
if(session.getAttribute("verify")!=null){
	session.removeAttribute("verify");
}
session.setAttribute("verify",verify);
%>

<div class="bg_one"></div>
<div align="center" class="main">
	<div class="inner-container">

		<div class="zoho_logo"></div>	
<form action="verify" method="post">
<%if("mail".equals(verify)){ %>
<input type="text" name="v_mail" placeholder="Mail ID"/>
<% }
if("phone".equals(verify)){%>
<input type="text" name="v_phno" placeholder="Phone Number"/>
<% }%>
<input type="submit" value="NEXT"/>
	<a id= "underline" class="zlogin" href="signin.jsp">Back to LOGIN</a>
</form>

</div>
</div>
</header>
</body>
</html>