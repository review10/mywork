<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create a Zoho Account</title>
<style>
.main {
    display: block;
    width: 560px;
    height: auto;
    background-color: #fff;
    box-shadow: 0px 2px 30px #ccc6;
    margin: auto;
    position: relative;
    z-index: 1;
    margin-top: 3%;
    overflow: hidden;
}
.inner-container {
    padding: 50px 50px;
    text-align: left;
    overflow: auto;
}
.zoho_logo {
    display: block;
    height: 30px;
    width: auto;
    margin-bottom: 20px;
    background: url(images/zoho-logo-zh.png) no-repeat transparent;
    background-size: auto 100%;
}
.signuptitle{
    display: block;
    font-size: 24px;
    font-weight: 500;
    margin-bottom: 30px;
    line-height: 30px;
    transition: all .1s ease-in-out;
}
input[type="text"], input[type="email"], input[type="password"], input[type="number"], textarea, select {
    display: block;
    width: 100%;
    height: 44px;
    box-sizing: border-box;
    border-radius: 2px;
    text-indent: 12px;
    font-size: 16px;
    outline: none;
    border: none;
    padding-right: 12px;
    transition: all .2s ease-in-out;
    background: #f8f8f8;
    border: 1px solid #E4E4E4;
    margin-top: 25px;
}
#firstname, #lastname {
    display: inline-block;
    margin-top: 0px;
    padding: 1px 2px;
}
.bg_one {
    display: block;
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    width: 100%;
    background: url(images/bg.svg) transparent;
    background-size: auto 100%;
    z-index: -1;
}
input[type=submit] {
    border-radius: 2px;
    margin-top: 25px;
    padding-right: 12px;
    cursor: pointer;
    display: block;
    width: 100%;
    height: 44px;
    border-radius: 4px;
    letter-spacing: .5px;
    font-size: 14px;
    font-weight: 600;
    outline: none;
    border: none;
    text-transform: uppercase;
    margin-bottom: 30px;
    transition: all .2s ease-in-out;
    box-shadow: 0px 2px 2px #fff;
    background-color: #159AFF;
    color: #fff;
    
}
</style>
</head>
<body>
<%
String reg_msg="";
reg_msg=(String)session.getAttribute("reg_msg");
%>
<div class="bg_one"></div>
<div align="center" class="main">
	<div class="inner-container">
		<div class="zoho_logo"></div>
		<div class="signuptitle">Create your Zoho account</div>
<form action="signup" method="post">
<%if(reg_msg!=null)

out.println(reg_msg);%>
<input type="text" placeholder="First Name" name="firstname" maxlength="100" id="firstname"/>
<input type="text" name="lastname" placeholder="Last Name"/>
<input type="text" name="mail_id" placeholder="Email Address"/>
<input type="text" name="ph_no" maxlength="10" placeholder="Phone Number"/>
<input type="text" name="country" placeholder="Country"/>
<input type="password" name="password" placeholder="Password"/>
<input type="submit" value="Sign Up"/>

</form>
</div>
</div>
</body>
</html>