package org;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/changetype")
public class PrivilegesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String g_name=request.getParameter("g_name");
		String g_type=request.getParameter("g_type");
		Org.Privileges(g_name,g_type);
		response.sendRedirect("showorg.jsp?g_name="+g_name);
	}
}
