package org;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class Org {
	public static ResultSet showOrg(int user_id) {
		ResultSet rs=null;
		CallableStatement procstate=null;
		Statement stat=null;
		try{
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			procstate =myConnection.prepareCall("{call myproc(?)}"); 
			procstate.setInt(1, user_id);
			procstate.execute();
			stat=myConnection.createStatement();
			rs=stat.executeQuery("Select * from n_table");
		}
		catch(Exception e){
			System.out.println("error occured in showorg");
			System.out.println(e.getMessage());
		}
		return rs;
	}
	public static ResultSet showRequests() {
		String fetch_req="Select * from requests";
		ResultSet rs=null;
		try {
			InitialContext ic=new InitialContext();
			Context ctx=(Context)ic.lookup("java:comp/env");
			DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat= myConnection.createStatement();
			rs=stat.executeQuery(fetch_req);
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return rs;
	}
	public static ResultSet getMembers(String org_name) {
		String fetch="Select * from "+org_name;
		ResultSet rs=null;
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat= myConnection.createStatement();
			rs=stat.executeQuery(fetch);
		}
		catch(Exception e){
			System.out.println("error occured in getmembers");
			System.out.println(e.getMessage());
		}
		return rs;
	}
	public static String getType(String org_name) {
		String fetch_org_type="Select g_type from temp_data where g_names=?";
		String g_type="";
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_org_type);
			stat.setString(1,org_name);
			ResultSet rs=stat.executeQuery();
			while(rs.next())
				g_type=rs.getString("g_type");
		}
		catch(Exception e){
			System.out.println("error occured in gettype");
			System.out.println(e.getMessage());
		}
		return g_type;
			
	}
	public static String getMemberType(int user_id) {
		String fetch_member_type="Select u_type from registration where user_id=?";
		String u_type="";
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_member_type);
			stat.setInt(1, user_id);
			ResultSet rs=stat.executeQuery();
			while(rs.next())
				u_type=rs.getString("u_type");
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
		return u_type;
	}
	public static String Create(String orgname,String orgtype) {
		String orgmsg="";
		try{
			
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			CallableStatement procstate=null;
			ResultSet rs=stat.executeQuery("select * from temp_data where g_names='"+orgname+"' "); 
			while(rs.next()) {
				orgmsg="exists";
			}
			stat.executeUpdate("INSERT INTO temp_data(g_names,g_type) VALUES('"+orgname+"','"+orgtype+"')");
			procstate = myConnection.prepareCall("{call createtable(?)}"); 
			procstate.setString(1,orgname);
			procstate.execute();
			orgmsg="done";
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
		return orgmsg;
	}
	public static void Delete(String[] orgs) {
		try{
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			for(int i=0; i<orgs.length; i++){
				stat.executeUpdate("drop table "+orgs[i]);
				stat.executeUpdate("delete from temp_data where g_names="+orgs[i]);
			}
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
	}
	public static void ApproveRequests(String req_name,int req_id) {
		String del_entry="delete from requests where user_id=? and g_name=?";
		String add_mem="insert into "+req_name+"(user_id) select * from(select ? as user_id) as t where not exists(select user_id from "+req_name+" where user_id=?) limit 1";
		try {
		InitialContext ic=new InitialContext();
	    Context ctx=(Context)ic.lookup("java:comp/env");
	    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
		Connection myConnection=ds.getConnection();
		
		PreparedStatement stat=myConnection.prepareStatement(del_entry);
		stat.setInt(1,req_id);
		stat.setString(2,req_name);
		stat.executeUpdate();
		
		PreparedStatement stat2=myConnection.prepareStatement(add_mem);
		stat2.setInt(1,req_id);
		stat2.setInt(2,req_id);
		stat2.executeUpdate();
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
		
	}
	public static ResultSet UserDetails(int user_id) {
		String fetch_details="Select firstname,lastname,ph_no,country,password,email_id,u_type from registration where user_id=?";
		ResultSet rs=null;
	try{	
		InitialContext ic=new InitialContext();
	    Context ctx=(Context)ic.lookup("java:comp/env");
	    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
		Connection myConnection=ds.getConnection();
		PreparedStatement stat=myConnection.prepareStatement(fetch_details);
		stat.setInt(1,user_id);
		rs=stat.executeQuery();
	}
	catch(Exception e){
		System.out.println("error occured in getmember type");
		System.out.println(e.getMessage());
	}
	return rs;
	}
	public static void Privileges(String org_name,String org_type) {
		try{
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			if(org_type.equals("private"))
				stat.executeUpdate("update temp_data set g_type="+"'public'"+" where g_names='"+org_name+"'");
			else
				stat.executeUpdate("update temp_data set g_type="+"'private'"+" where g_names='"+org_name+"'");	
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
	}
	public static void RemoveMembers(String[] selected_users,String org_name) {
		try{
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=null;
			int vals;
			String remove_q="";
			for(int i=0; i<selected_users.length; i++){
				vals=Integer.parseInt(selected_users[i]);
				remove_q="delete from "+org_name+" where user_id=?";
				stat=myConnection.prepareStatement(remove_q);
				stat.setInt(1,vals);
				stat.executeUpdate();
			}
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
	}
	public static String Join(int user_id,String u_type,String g_name) {
		String res="";
		String join_q="INSERT INTO "+g_name+"(user_id) VALUES(?)";
		String search_q="Select user_id from "+g_name+" where user_id=?";
		String fetch_type="Select g_type from temp_data where g_names=?";
		String insert_req="insert into requests(user_id,g_name) select * from(select ? as user_id,'"+g_name+"' as g_name) as t where not exists(select user_id from requests where user_id=? and g_name='"+g_name+"') limit 1";
		try{	
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(search_q);
			stat.setInt(1,user_id);
			ResultSet rs=stat.executeQuery();
			if(rs.next()) {
				PreparedStatement stat2=myConnection.prepareStatement(fetch_type);
				stat2.setString(1,g_name);
				ResultSet rs2=stat.executeQuery();
				if(rs2.next()) {
					if("public".equals(rs.getString(1))) {
						PreparedStatement stat3=myConnection.prepareStatement(join_q);
						stat3.setInt(1,user_id);
						stat3.executeUpdate();
						res="done";
					}
					else {
						if(u_type.equals("User")){
							PreparedStatement stat4=myConnection.prepareStatement(insert_req);
							stat4.setInt(1,user_id);
							stat4.setInt(2,user_id);
							stat4.executeUpdate();
							res="requestadmin";
						}
						else{
							PreparedStatement stat3=myConnection.prepareStatement(join_q);
							stat3.setInt(1,user_id);
							stat3.executeUpdate();
							res="done";
						}
						
					}
				}
			}
			else
				res="exists";
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
		return res;
	}
	public static ResultSet usersNotInOrg(String g_name) {
		ResultSet rs=null;
		try{	
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			String fetch_users="select user_id from registration where u_type='User' and user_id not in (select user_id from "+g_name+")";
			Statement stat=myConnection.createStatement();
			rs=stat.executeQuery(fetch_users);
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
		return rs;
		
	}
	public static void addUser(String[] s,String g_name ) {
		try{
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			for (int i=0;i<s.length;i++) {
				int v= Integer.parseInt(s[i]);
				String q="INSERT INTO "+g_name+" VALUES("+v+")";
				stat.executeUpdate(q);
			}
		}
		catch(Exception e){
			System.out.println("error occured in getmember type");
			System.out.println(e.getMessage());
		}
	}
}
