package org;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
@WebServlet("/remove_users")
public class RemoveMembersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();  
		String org_name=(String)session.getAttribute("org_name");
		String selected_users[]=request.getParameterValues("selected_users");
		Org.RemoveMembers(selected_users, org_name);
		response.sendRedirect("showorg.jsp?g_name="+org_name);
	}

}
