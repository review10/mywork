package org;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
@WebServlet("/join")
public class JoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(); 
		String org_name=(String)session.getAttribute("org_name");
		String u_type=(String)session.getAttribute("u_type");
		int user_id=(int)session.getAttribute("user_id");
		String joinmsg=Org.Join(user_id,u_type,org_name);
		response.sendRedirect("showorg.jsp?joinmsg="+joinmsg+"&g_name="+org_name);
	}
}
