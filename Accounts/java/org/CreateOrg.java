package org;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
@WebServlet("/neworg")
public class CreateOrg extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orgname=request.getParameter("orgname");
		String orgtype=request.getParameter("orgtype");
		String orgmsg=Org.Create(orgname,orgtype);
		response.sendRedirect("createorg.jsp?orgmsg="+orgmsg);
	}
}
