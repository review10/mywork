package org;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/approve")
public class ApproveRequestsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String req_name=request.getParameter("req_name");
		int req_id=Integer.parseInt(request.getParameter("req_id"));
		Org.ApproveRequests(req_name,req_id);
		response.sendRedirect("myorgs.jsp");
	}
}
