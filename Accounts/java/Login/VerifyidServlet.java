package Login;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/verifyid")
public class VerifyidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		int v_code=Integer.parseInt(request.getParameter("v_code"));
		String verify=(String)session.getAttribute("verify");
		String v_mail=(String)session.getAttribute("v_mail");
		String v_phno=(String)session.getAttribute("v_phno");
		int user_id=Validate.verificationCode(verify,v_code,v_mail,v_phno);
		if(user_id!=0) {
			ResultSet rs=Validate.getFirstName(user_id);
			try {
			session.setAttribute("firstname",rs.getString("firstname"));
			session.setAttribute("u_type",rs.getString("u_type"));
			}
			catch(Exception e){
				System.out.println("error occured");
				System.out.println(e.getMessage());
			}
			session.setAttribute("user_id",user_id);
			response.sendRedirect("welcome.jsp");
		  
		}
		else {
			if(v_mail=="") 
				response.sendRedirect("verifyid.jsp?verify=phno&error=wrong_code");
			else
				response.sendRedirect("verifyid.jsp?verify=mail&error=wrong_code");
		}	
	}
}
