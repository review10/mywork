package Login;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/verify")
public class VerifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String verify=(String) session.getAttribute("verify");
		if("mail".equals(verify)){
			String v_mail=request.getParameter("v_mail");
			if(Validate.checkMailExists(v_mail)==1) {
				session.setAttribute("v_mail",v_mail);
				session.setAttribute("v_phno","");
				response.sendRedirect("verifyid.jsp");
			}
			else {
				response.sendRedirect("other_signin.jsp?verify=mail&error=wrong_email");
			}
				
		}
		else {
			String v_phno=request.getParameter("v_phno");
			if(Validate.checkPhnoExists(v_phno)==1) {
				session.setAttribute("v_phno",v_phno);
				session.setAttribute("v_mail","");
				response.sendRedirect("verifyid.jsp");
			}
			else {
				response.sendRedirect("other_signin.jsp?verify=phone&error=wrong_phno");
			}
		}

	}

}
