package Login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import  java.sql.*;
public class Register {
	public static int insert(String firstname,String lastname,String mail_id,String ph_no,String country,String password){
		String insertion="INSERT INTO registration (firstname,lastname,email_id,ph_no,country,password,u_type) VALUES(?,?,?,?,?,?,'User')";
		String fetch_userid="SELECT user_id FROM registration WHERE firstname=? and lastname=? and email_id=? and ph_no=? and country=? and password=?";
		String  insert_verifymail="insert into user_mail values(?,?, LPAD(FLOOR(RAND() * 999999.99), 6, '0'))";
		String insert_verifyphno="insert into user_phno values(?,?,LPAD(FLOOR(RAND() * 999999.99), 6, '0'))";
		int user_id=0;
		try {
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(insertion);
			stat.setString(1,firstname);
			stat.setString(2,lastname);
			stat.setString(3,mail_id);
			stat.setString(4,ph_no);
			stat.setString(5,country);
			stat.setString(6,password);
			stat.execute();
			PreparedStatement stat2=myConnection.prepareStatement(fetch_userid);
			stat2.setString(1,firstname);
			stat2.setString(2,lastname);
			stat2.setString(3,mail_id);
			stat2.setString(4,ph_no);
			stat2.setString(5,country);
			stat2.setString(6,password);
			ResultSet myResultSet2=stat2.executeQuery();
			if(myResultSet2.next())
				user_id=myResultSet2.getInt("user_id");
			PreparedStatement stat3=myConnection.prepareStatement(insert_verifymail);
			stat3.setInt(1,user_id);
			stat3.setString(2,mail_id);
			stat3.execute();
			PreparedStatement stat4=myConnection.prepareStatement(insert_verifyphno);
			stat4.setInt(1,user_id);
			stat4.setString(2,ph_no);
			stat4.execute();
			
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return user_id;
	
	}
}
