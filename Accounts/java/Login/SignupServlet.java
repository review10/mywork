package Login;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
@WebServlet("/signup")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String firstname=request.getParameter("firstname");
		String lastname=request.getParameter("lastname");
		String mail_id=request.getParameter("mail_id");
		String ph_no=request.getParameter("ph_no");
		String country=request.getParameter("country");
		String password=request.getParameter("password");
		
		Pattern mail_pattern = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$"); 
		Matcher matcher = mail_pattern.matcher(mail_id); 
		Pattern phno_pattern = Pattern.compile("^\\d{10}$");
		Matcher m = phno_pattern.matcher(ph_no);
		if(matcher.matches() && m.matches()){
			int user_id=Register.insert(firstname, lastname, mail_id, ph_no, country, password);
			System.out.println(user_id);
			if(user_id!=0) {
				session.setAttribute("firstname",firstname);
				session.setAttribute("user_id",user_id);
				session.setAttribute("u_type","User");
				response.sendRedirect("welcome.jsp");
			}
		}
		else {
			if(matcher.matches()==false){
				if(m.matches()==false){
					session.setAttribute("reg_msg","Enter valid Email Id and phone number");
					response.sendRedirect("signup.jsp");
				}
				else{
					session.setAttribute("reg_msg","Enter valid Email Id");
					response.sendRedirect("signup.jsp");
				}	
			}
			else{
				session.setAttribute("reg_msg","Enter valid phone number");
				response.sendRedirect("signup.jsp");
			}
		}
	}
}
