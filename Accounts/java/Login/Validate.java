package Login;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.*;

public class Validate{
	public static boolean isUserregistered(int user_id){
		boolean flag=false;
		String fetch_user="SELECT firstname FROM  registration WHERE user_id=?";
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_user);
			stat.setInt(1,user_id);
			ResultSet myResultSet=stat.executeQuery();
			flag=myResultSet.next();
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return flag;
	}
	public static ResultSet checkPassword(int user_id,String password){
		String fetch_firstname="SELECT firstname,u_type FROM  registration WHERE user_id=? and password=?";
		ResultSet rs=null;
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_firstname);
			stat.setInt(1,user_id);
			stat.setString(2,password);
			rs=stat.executeQuery();
			
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return rs;
	}
	public static int checkMailExists(String v_mail) {
		String checkif_mail_exists="Select if(exists(select * from user_mail where email=?),1,0)";
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(checkif_mail_exists);
			stat.setString(1,v_mail);
			ResultSet myResultSet=stat.executeQuery();
			if(myResultSet.next())
			return myResultSet.getInt(1);
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return 0;
	}
	public static int checkPhnoExists(String v_phno) {
		String checkif_phno_exists="Select if(exists(select * from user_phno where ph_no='"+v_phno+"'),1,0)";
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			ResultSet rs=stat.executeQuery(checkif_phno_exists);
			if(rs.next())
			return rs.getInt(1);
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return 0;
	}
	public static int verificationCode(String verify,int v_code,String v_mail,String v_phno) {
		String login_mail="select user_id from user_mail where email='"+v_mail+"' and verify_id=?";
		String login_phno="select user_id from user_phno where ph_no='"+v_phno+"' and verify_id=?";
		int user_id=0;
		PreparedStatement stat=null;
		try {
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			if(verify.equals("mail")) 
				stat=myConnection.prepareStatement(login_mail);
			else 
				stat=myConnection.prepareStatement(login_phno);
			stat.setInt(1,v_code);
			ResultSet rs=stat.executeQuery();
			while(rs.next())
				user_id = rs.getInt(1);
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return user_id;
	}
	public static ResultSet getFirstName(int user_id) {
		String fetch_firstname="select firstname,u_type from registration where user_id=?";
		ResultSet rs=null;
		try {
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_firstname);
			stat.setInt(1,user_id);
			rs=stat.executeQuery();
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return rs;
	}
	
} 

