package Login;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();  
		response.setContentType("text/html;charset=UTF-8");
		String user_id=request.getParameter("user_id");
		String password=request.getParameter("password");
		String userid_p=user_id+"p";
		try{
			int flag=0;
			int flag2=0;
			Cookie []cks=request.getCookies();
			for(int i=0;i<cks.length;i++){
				if(cks[i].getName().equals(userid_p)){
					flag=1;
					if(cks[i].getValue().equals(password)){
						flag2=1;
						for(int j=0;j<cks.length;j++){
							if(cks[j].getName().equals(user_id)){
								session.setAttribute("firstname",cks[j].getValue());
								session.setAttribute("user_id",Integer.parseInt(user_id));
								ResultSet rs=Validate.getFirstName(Integer.parseInt(user_id));
								if(rs.next()) {
								session.setAttribute("u_type",rs.getString("u_type"));
								response.sendRedirect("welcome.jsp");
								}
							}
						}
					}
				}
			}
			if(flag2==0 && flag==1){
				session.setAttribute("login_msg","Wrong Password");
				response.sendRedirect("signin.jsp");
			}
			if(flag==0){
				if(Validate.isUserregistered(Integer.parseInt(user_id))){
					ResultSet res=Validate.checkPassword(Integer.parseInt(user_id),password);
					String firstname=null;
					String u_type=null;
					while(res.next()) {
					firstname=res.getString("firstname");
					u_type=res.getString("u_type");
					System.out.println(firstname);
					}
					if(firstname!="") {
						Cookie c1 = new Cookie(userid_p,password);
						Cookie c=new Cookie(user_id,firstname);
						c.setMaxAge(20 * 60 * 1000);
						c1.setMaxAge( 20 * 60 *1000);
						response.addCookie(c);
						response.addCookie(c1);
						session.setAttribute("firstname",firstname);
						session.setAttribute("u_type",u_type);
						session.setAttribute("user_id",Integer.parseInt(user_id));
						response.sendRedirect("welcome.jsp");
					}
					else{
						session.setAttribute("login_msg","Wrong Password");
						response.sendRedirect("signin.jsp");
					}
				}
				else{
					session.setAttribute("login_msg","Not Registered");
					response.sendRedirect("signin.jsp");	
				}
			}
			
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
	}
}
