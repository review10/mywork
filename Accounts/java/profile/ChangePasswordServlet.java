package profile;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
@WebServlet("/change_password")
public class ChangePasswordServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();  
		int user_id=(int)session.getAttribute("user_id"); 
		String oldpassword=request.getParameter("oldpassword");
		String newpassword=request.getParameter("newpassword");
		String confirmpassword=request.getParameter("confirmpassword");
		if(!confirmpassword.equals(newpassword))
			response.sendRedirect("changepassword.jsp?msg=notMatch");
		else{
			if(Profile.verifyOldPassword(user_id,oldpassword)) {
				Profile.changePassword(user_id,newpassword);
				response.sendRedirect("changepassword.jsp?msg=done");
			}
			else
				response.sendRedirect("changepassword.jsp?msg=wrong");
		}
	}
}
