package profile;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Profile {
	public static String[] showProfile(int user_id){
		String fetch_f_q="SELECT firstname FROM  registration WHERE user_id=?";
		String fetch_l_q="SELECT lastname FROM  registration WHERE user_id=?";
		String fetch_m_q="SELECT email_id FROM  registration WHERE user_id=?";
		String fetch_c_q="SELECT country FROM  registration WHERE user_id=?";
		String fetch_p_q="SELECT ph_no FROM  registration WHERE user_id=?";
		String[] res=new String[5];
		try{
			InitialContext ic=new InitialContext();
        	Context ctx=(Context)ic.lookup("java:comp/env");
        	DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			PreparedStatement stat=myConnection.prepareStatement(fetch_f_q);
			stat.setInt(1,(user_id));
			ResultSet myResultSet=stat.executeQuery();
			while(myResultSet.next())
			res[0]=myResultSet.getString(1);
			
			PreparedStatement stat1=myConnection.prepareStatement(fetch_l_q);
			stat1.setInt(1,(user_id));
			myResultSet=stat1.executeQuery();
			while(myResultSet.next())
			res[1]=myResultSet.getString(1);
			
			PreparedStatement stat2=myConnection.prepareStatement(fetch_m_q);
			stat2.setInt(1,(user_id));
			myResultSet=stat2.executeQuery();
			while(myResultSet.next())
			res[2]=myResultSet.getString(1);
			
			PreparedStatement stat4=myConnection.prepareStatement(fetch_p_q);
			stat4.setInt(1,(user_id));
			myResultSet=stat4.executeQuery();
			while(myResultSet.next())
			res[4]=myResultSet.getString(1);
			
			PreparedStatement stat3=myConnection.prepareStatement(fetch_c_q);
			stat3.setInt(1,(user_id));
			myResultSet=stat3.executeQuery();
			while(myResultSet.next())
			res[3]=myResultSet.getString(1);
			
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return res;
	}
	public static boolean verifyOldPassword(int user_id,String password){
		try{	
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			ResultSet rs=stat.executeQuery("select user_id from registration where user_id='"+user_id+"' and password='"+password+"'");
			if(rs.next())
				return true;
			else
				return false;
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static void changePassword(int user_id,String password) {
		try {
			InitialContext ic=new InitialContext();
		    Context ctx=(Context)ic.lookup("java:comp/env");
		    DataSource ds=(DataSource)ctx.lookup("jdbc/web");	
			Connection myConnection=ds.getConnection();
			Statement stat=myConnection.createStatement();
			stat.executeUpdate("update registration set password='"+password+"' where user_id='"+user_id+"'");
		}
		catch(Exception e){
			System.out.println("error occured");
			System.out.println(e.getMessage());
		}
	}
}
