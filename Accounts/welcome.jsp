<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,java.util.*"%>

<html>
<head>
<style>
#underline{
            text-decoration: none;
        }

.zlogin {
    background: #f0483e;
    color: #fff;
    display: inline-block;
    font-size: 18px;
    font-weight: 600;
    height: 50px;
    line-height: 50px;
    padding: 0 40px;
    text-align: center;
    text-transform: uppercase;
    box-sizing: border-box;
    margin-top: 10px;
}
.center{
    text-align: center;
    position:absolute;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; 
    margin-left: -15em; 
}
.vertical-menu {
margin: 15px 0px;
    margin-left: 50px;
  width: 200px;
}

.vertical-menu a {
  background-color: #011722;
  color: white;
  display: block;
  padding: 12px;
  text-decoration: none;
}

.vertical-menu a:hover {
  background-color:  #ffffff08;
}

.vertical-menu a.active {
  background-color: #ffffff08;
  color: white;
}
.navbar {
    height: 100%;
    width: 300px;
    display: block;
    position: fixed;
    left: 0px;
    top: 54px;
    z-index: 5;
    background-color: #011722;
    transition: all .2s ease-in-out;
}
.ztopbar {
    display: block;
    height: 54px;
    width: 100%;
    background-color: #fff;
    position: fixed;
    z-index: 5;
    box-shadow: 0px 0px 2px #ccc;
    top: 0px;
}
.zoho_logo {
    display: inline-block;
    height: 24px;
    width: 168px;
    margin: 15px 0px;
    margin-left: 50px;
    background: url(images/zoho-accounts-logo.png) no-repeat transparent;
    background-size: auto 100%;
    cursor: pointer;
}
</style>
</head>
<body>
<%
int user_id=(int)session.getAttribute("user_id");
String firstname=(String)session.getAttribute("firstname");
String u_type=(String)session.getAttribute("u_type");
%>
<div class="ztopbar">
<span class="zoho_logo" style="margin-left:50px;"></span>
</div>
<div class="center">
<header>
	<h2>Welcome! <%= firstname %></h2>
</header>
<p>User-id :<%= user_id %></p>	
<a id= "underline" class="zlogin" href="signin.jsp">LOG OUT</a>
</div>
<div class="navbar" style="height=100%;overflow:unset;">
<div class="vertical-menu">
  <a href="welcome.jsp" class="active">Home</a>
  <a href="profile.jsp">Profile</a>
  <a id= "underline"  href="myorgs.jsp" >My Orgs</a>
  <%if(("Admin".equals(u_type)) || ("SuperAdmin".equals(u_type))){%>
  <a href="admin.jsp">Admin</a>
  <%}%>
</div>
</div>
</body>
</html>