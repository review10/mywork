<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,java.util.*,org.Org"%>

<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.icon{
    box-sizing: border-box;
    width: 100%;
    padding: 8px;
    justify-content: space-between;
    float:right;
    text-align:right;
}
.search-container {
  float: right;
  display: block;
    text-align: left;
    font-size: 17px;
    border-color:solid #009879;
   
}
#underline{
            text-decoration: none;
        }
.content-table{
	border-collapse:collapse;
	margin:25px 0;
	font-size:0.9em;
	min-width:400px;
	border-radius:5px 5px 0 0;
	overflow:hidden;
	box-shadow: 0 0 20px rgba(0,0,0,0.15);
	margin-left: auto;
        margin-right: auto;
}
.content-table thead tr{
	background-color:#009879;
	color:#ffffff;
	text-align:center;
	font-weight:bold;
}
.content-table thead th, .content-table td{
	padding: 12px 15px;
	text-align:center;
	
}
.content-table tbody tr{
	border-bottom:1px solid #dddddd;
}
.content-table tbody tr:last-of-type{
	border-bottom:2px solid #009879;
}
#underline{
            text-decoration: none;
        }
.search-container input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size:15px;
  border: 1px solid #9aa0a6;
}

</style>
</head>
<body>
<%
int user_id=(int)session.getAttribute("user_id");
String u_type=(String)session.getAttribute("u_type");
%>
<div class="search-container">
    <form action="searchorg.jsp">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search" style="border: 1px solid #9aa0a6;font-size:15px"></i></button>
    </form>
</div>
<div class="icon">
<a id= "underline"  href="createorg.jsp?user_id=${user_id}">Create Org</a>
</div>


<form action="deleteorgs">
<TABLE class="content-table">
      <thead>
      	  <tr>
      	     <th>&nbsp; </th>
            <th>My Orgs</th>
          </tr>
      </thead>
      
      <tbody>
      <%
      ResultSet rs=Org.showOrg(user_id);
      if(rs!=null){
      while(rs.next()){ 
      String org_name=rs.getString(1);
      %>
     	 <tr>
     	 <% if(u_type.equals("User")){ %>
     	 <td> </td>
         <td><a id="underline" href="showorg.jsp?org_name=<%=org_name%>" ><%= org_name%></a></td>
          <%}
         else{%>
         	<td><input type="checkbox" name="selected_orgs" value=<%=org_name%>></td>
         	<td><a id="underline" href="showorg.jsp?org_name=<%=org_name%>" ><%= org_name%></a></td>
         	
         <%}%>
       </tr>
       <% } }%>
     </tbody>
</TABLE>
<%if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin"))){%>
<input type="submit" value="Delete Orgs">
<%}%>
</form>
<%if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin"))){
	
	ResultSet rs2=Org.showRequests();
%>
<TABLE class="content-table">
<caption>Requests</caption>
      <thead>
          <tr>
            <th>User ID</th>
            <th>Org Name</th>
            <th> </th>
          </tr>
      </thead>
      <tbody>
      <% while(rs2.next()){ 
      int req_id=rs2.getInt(1);
      String req_name=rs2.getString(2);
      %>
     	 <tr>
         <td><%= req_id%></td>
         <td><%=req_name%></td> 
         <td><a id= "underline" class="zlogin" href="approve?req_id=<%=req_id%>&req_name=<%=req_name%>">Approve</a></td>
       </tr>
       <% } %>
     </tbody>
</TABLE>
<%} %>>
</body>
</html>