<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,java.util.*,org.Org"%>

<html>
<head>
<style>
.content-table{
	border-collapse:collapse;
	margin:25px 0;
	font-size:0.9em;
	min-width:400px;
	border-radius:5px 5px 0 0;
	overflow:hidden;
	box-shadow: 0 0 20px rgba(0,0,0,0.15);
	margin-left: auto;
        margin-right: auto;
}
.content-table thead tr{
	background-color:#009879;
	color:#ffffff;
	text-align:center;
	font-weight:bold;
}
.content-table thead th, .content-table td{
	padding: 12px 15px;
	text-align:center;
	
}
.content-table tbody tr{
	border-bottom:1px solid #dddddd;
}
.content-table tbody tr:last-of-type{
	border-bottom:2px solid #009879;
}

</style>
</head>
<body>
<% 
int g_member=Integer.parseInt(request.getParameter("g_member"));
ResultSet rs=Org.UserDetails(g_member);

%>

<TABLE class="content-table">
      <thead>
      	  <tr>
      	    <th>User Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Country</th>
            <th>Password</th>
            <th>Email Id</th>
            <th>User Type</th>
          </tr>
      </thead>
      
      <tbody>
	<tr>
         <td><%= g_member%></td>
     
       <%if(rs.next()) { %>
      
         <td><%=rs.getString("firstname")%></td>     
         <td><%=rs.getString("lastname")%></td>
         <td><%=rs.getString("ph_no")%></td>   
         <td><%=rs.getString("country")%></td>   
         <td><%=rs.getString("password")%></td>     
         <td><%=rs.getString("email_id")%></td>
         <td><%=rs.getString("u_type")%></td>
         <% }%>
       </tr>

     </tbody>
</TABLE>
</body>

