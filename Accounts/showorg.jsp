<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.sql.DataSource,javax.naming.*,org.apache.commons.dbcp2.BasicDataSource,java.util.*,org.Org"%>
<html>
<head>
<style>
p{
	margin:25px 0;
    text-align: center;
    position:center;
    
}
.icon{
    box-sizing: border-box;
    width: 100%;
    padding: 8px;
    justify-content: space-between;
    float:right;
    text-align:right;
}
#underline{
            text-decoration: none;
        }
.content-table{
	border-collapse:collapse;
	margin:25px 0;
	font-size:0.9em;
	min-width:400px;
	border-radius:5px 5px 0 0;
	overflow:hidden;
	box-shadow: 0 0 20px rgba(0,0,0,0.15);
	margin-left: auto;
        margin-right: auto;
}
.content-table thead tr{
	background-color:#009879;
	color:#ffffff;
	text-align:center;
	font-weight:bold;
}
.content-table thead th, .content-table td{
	padding: 12px 15px;
	text-align:center;
	
}
.content-table tbody tr{
	border-bottom:1px solid #dddddd;
}
.content-table tbody tr:last-of-type{
	border-bottom:2px solid #009879;
}
#underline{
            text-decoration: none;
        }
.zlogin {
    background: #f0483e;
    color: #fff;
    display: inline-block;
    font-size: 12px;
    font-weight: 600;
    height: 30px;
    line-height: 30px;
    padding: 0 20px;
    text-align: center;
    text-transform: uppercase;
    box-sizing: border-box;
    margin-left: 40%;
    overflow:hidden;
}
.z2login {
    background: #f0483e;
    color: #fff;
    display: inline-block;
    font-size: 12px;
    font-weight: 600;
    height: 30px;
    line-height: 30px;
    padding: 0 20px;
    text-align: center;
    text-transform: uppercase;
    box-sizing: border-box;
    margin-left: 13%;
    overflow:hidden;
}
.alert {
  opacity: 1;
  transition: opacity 0.6s; /* 600ms to fade out */
}
.zlogin {
	text-decoration: none;
        background-color: #10bc83;
    color: #fff;
    display: inline-block;
    box-sizing: border-box;
    margin-top: 10px;
    font-size: 14px;
    font-weight: 500;
    line-height: 40px;
    position: relative;
    text-align: center;
    border-radius: 4px;
    padding: 0px 30px;
    overflow: hidden;
    float:left;
}
</style>
</head>
<body>
<header>
<%
String org_name=request.getParameter("org_name");
session.setAttribute("org_name",org_name);
String u_type=(String)session.getAttribute("u_type");
int user_id=(int)session.getAttribute("user_id");  
String org_type=Org.getType(org_name);
ResultSet rs=Org.getMembers(org_name);
int org_member;
String org_mem_type;
%>

<%
String joinmsg=request.getParameter("joinmsg");
if("exists".equals(joinmsg)){
%>
<h3 class="alert">Already in the Org!</h3>
<%}

if("done".equals(joinmsg)){
%>
<h3 class="alert">Successfully Joined!</h3>
<%}
if("requestadmin".equals(joinmsg)){
%>
<h3 class="alert">Request sent to admin!</h3>
<%}%>


<p><%= org_type %></p>

<% if((u_type.equals("Admin")) || (u_type.equals("SuperAdmin"))){
	if(org_type.equals("public")){ %>
		<a class="zlogin" href="changetype?g_name=<%=org_name%>&g_type=<%=org_type%>">Make private</a>
	<%}else{ %>
		<a class="zlogin" href="changetype?g_name=<%=org_name%>&g_type=<%=org_type%>">Make public</a>
<%}}%>
<form action="remove_users">
<TABLE class="content-table">
      <thead>
      	  <tr>
      	    <th>&nbsp; </th>
            <th><%=org_name%></th>
          </tr>
      </thead>
      
      <tbody>
      <%
	while(rs.next()){
		org_member=rs.getInt(1);
		org_mem_type=Org.getMemberType(org_member);
		if("Admin".equals(u_type)){
			if(("Admin".equals(org_mem_type)) || ("SuperAdmin".equals(org_mem_type))){  %>
	   			<tr>
	   				<td> </td>
	   				<td><%= org_member%></td>
	   			</tr>
			<%}%>
			<%if( ("User".equals(org_mem_type))){%>
				<tr>
					<td><input type="checkbox" name="selected_users" value=<%=org_member%>></td>
         				<td><a id="underline" href="showdetails.jsp?g_member=<%=org_member%>" ><%= org_member%></a></td>
       			</tr>
       	<%}}%>
       
       	<%if("SuperAdmin".equals(u_type)){%>
       		<tr>
       			<td><input type="checkbox" name="selected_users" value=<%=org_member%>></td>
         			<td><a id="underline" href="showdetails.jsp?g_member=<%= org_member%>" ><%= org_member%></a></td>
      			</tr>
       
      		<%}if("User".equals(u_type)) {%>
     	 		<tr>
     	 			<td> </td>
         			<td><%= org_member%></td>
        		</tr>
       	<% }
       }%>
     </tbody>
</TABLE>
<%if( ("Admin".equals(u_type) )|| ("SuperAdmin".equals(u_type))){%>
<input type="submit" value="Remove members">
<%}%>
</form>

<a id= "underline" class="zlogin" href="join">JOIN</a>
<%if(u_type.equals("Admin") || u_type.equals("SuperAdmin")){%>
<a id= "underline" class="z2login" href="addusers.jsp">ADD</a>
<%
}
 %>
 </header>
</body>
</html>